<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    use HasFactory;

    public function customer()
    {
    	return $this->belongsTo(User::class, 'customer_id');
    }

    public function deliveryMan()
    {
    	return $this->belongsTo(User::class, 'deliveryMan_id');
    }

    public function ordersActionStatus()
    {
    	return $this->hasMany(OrderActionStatus::class);
    }
}
