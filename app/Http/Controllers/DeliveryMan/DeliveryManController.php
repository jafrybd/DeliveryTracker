<?php

namespace App\Http\Controllers\DeliveryMan;

use App\Http\Controllers\Controller;
use App\Http\Controllers\CommonController;
use App\Models\Orders;
use App\Models\User;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use \PDF;
use Validator;
use App\Models\OrderActionStatus;
use CreateOrderActionStatus;

class DeliveryManController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->common_class_obj = new CommonController();
    }
    //
    public function index()
    {
        $totalCompletedOrders = Orders::where([['deliveryMan_id' , '=',Auth::user()->id],['order_status', '=', 5], ['active_status', '=', 1]])->count();
         
        $totalRunningOrders = Orders::where([['deliveryMan_id' , '=',Auth::user()->id],['active_status', '=', 1]])->whereIn('order_status', [2,3])->count();
        
        $totalNewOrders = Orders::where([['deliveryMan_id' , '=',Auth::user()->id],['order_status', '=', 1], ['active_status', '=', 1]])->count();
        
        $runningOrderList = Orders::where([['active_status', '!=', 0], ["deliveryMan_id", "=", Auth::user()->id]])->orderBy('id', 'DESC')->limit(5)->get();

        return view('deliveryMan.deliveryManDashboard',compact('totalCompletedOrders','totalRunningOrders','totalNewOrders','runningOrderList'));
    }

    public function list()
    {
        $deliveryMans = User::where([['role_id', '=', 2], ['status', '=', 1]])->get();
        
        return view('deliveryMan.list',compact('deliveryMans'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request)
    {
        if(strlen($request->name) < 5 ||  strlen($request->name) >30){
            return redirect()->route('delivery-man.list')->with('error', 'Name should be at least 5 character and maximum 30 characters');
        }

        if(strlen($request->phone_no) != 11 ){
            return redirect()->route('delivery-man.list')->with('error', 'Phone Number should be 11 digits');
        }

        if (!filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
            return redirect()->route('delivery-man.list')->with('error', 'Invalid Email Format');
        }

        if(empty($request->address) ){
            return redirect()->route('delivery-man.list')->with('error', 'Address Should not be empty');
        }
        

        if (User::where([['email', '=', $request->email],['status','=',1]])->first()) {
            return redirect()->route('delivery-man.list')->with('error', 'This Email  ' . $request->input('email') . ' Already Used');
         }
        
         $defaultPassword = "12345678";

        $deliveryMan = new User();
        $deliveryMan->name = $request->name;
        $deliveryMan->phone_no = $request->phone_no;
        $deliveryMan->email = $request->email;
        $deliveryMan->address =  empty($request->address) ? "" : $request->address;
        $deliveryMan->role_id =  2;
        $deliveryMan->password = Hash::make($defaultPassword);

        if ($request->hasFile('photo')) { // if image is not empty.
            $request->validate([
                'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            $imageName = time() . '.' . $request->photo->extension();
            $request->photo->move(public_path('images/deliveryMan'), $imageName);
            $deliveryMan->photo = $imageName;
        }

        $deliveryMan->save();

        if ($deliveryMan) {
            return redirect()->route('delivery-man.list')->with('success', 'Delivery Man ' . $request->input('name') . ' Added Successfully');
        }
    }

    public function updateData(Request $request)
    {
        
        if(strlen($request->name) < 5 ||  strlen($request->name) >30){
            return redirect()->route('delivery-man.list')->with('error', 'Name should be at least 5 character and maximum 30 characters');
        }

        if(strlen($request->phone_no) != 11 ){
            return redirect()->route('delivery-man.list')->with('error', 'Phone Number should be 11 digits');
        }

        if (!filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
            return redirect()->route('delivery-man.list')->with('error', 'Invalid Email Format');
        }

        if(empty($request->address) ){
            return redirect()->route('delivery-man.list')->with('error', 'Address Should not be empty');
        }
        

        if (User::where([['email', '=', $request->email],['id','!=', $request->id],['status','=',1]])->first()) {
            return redirect()->route('delivery-man.list')->with('error', 'This Email  ' . $request->input('email') . ' Already Used');
         }
        
        $deliveryMan = User::findorFail($request->id);
        
        $deliveryMan->name = $request->name;
        $deliveryMan->phone_no = $request->phone_no;
        $deliveryMan->email = $request->email;
        $deliveryMan->address =  empty($request->address) ? "" : $request->address;
        

        if ($request->hasFile('photo')) { // if image is not empty.
           $existingImage = $deliveryMan->photo ;
           
           
            $request->validate([
                'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            
            if($existingImage != 'default.jpg'){

                if (file_exists('images/deliveryMan/' . $existingImage)) {
                    
                    unlink('images/deliveryMan/' . $existingImage);
                }
            }
            

            $imageName = time() . '.' . $request->photo->extension();
            $request->photo->move(public_path('images/deliveryMan'), $imageName);
            $deliveryMan->photo = $imageName;
        }

        $deliveryMan->save();

        if ($deliveryMan) {
            return redirect()->route('delivery-man.list')->with('success', 'Delivery Man ' . $request->input('name') . ' Upadated Successfully');
        }
    }


    public function delete($id)
    {
        $deliveryMan = User::findorFail($id);
        
        $deliveryMan->status = 0;
        $existingImage = $deliveryMan->photo ;

        if($existingImage != 'default.jpg'){

            if (file_exists('images/deliveryMan/' . $existingImage)) {
                
                unlink('images/deliveryMan/' . $existingImage);
            }
        }

        $deliveryMan->save();

        return redirect()->route('delivery-man.list')->with('success', 'Delivery Man ' . $deliveryMan->name . ' Deleted Successfully');
    }

    public function details($id)
    {
        $deliveryMan = User::findorFail($id);
        $runningOrderList = Orders::where([['active_status', '!=', 0], ["deliveryMan_id", "=", $id]])->orderBy('id', 'DESC')->get();


        $complete_order = 0;
        $running_order = 0;
        $cancel_order = 0;

        for ($i=0; $i < count($runningOrderList); $i++) { 
            if ($runningOrderList[$i]->order_status == 5) $complete_order++;
            else if ($runningOrderList[$i]->order_status == -1) $cancel_order++;
            else if ($runningOrderList[$i]->order_status > 0  && $runningOrderList[$i]->order_status < 5)  $running_order++;
        }

        return view('deliveryMan.details', compact('deliveryMan', 'id', 'runningOrderList', 'cancel_order', 'complete_order', 'running_order'));
    }
}
