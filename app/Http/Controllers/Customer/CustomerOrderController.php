<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\CommonController;
use App\Models\OrderActionStatus;
use App\Models\User;
use App\Models\Orders;
use Carbon\Carbon;
use CreateOrderActionStatus;
use Illuminate\Support\Facades\Auth;


class CustomerOrderController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->common_class_obj = new CommonController();
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function mapView(Request $request)
    {
        $id = (empty($request->id) || is_null($request->id)) ? 0 : $request->id;
        $user_role_id = Auth::user()->role_id;


        if ($user_role_id == 2) {
            $runningOrderList = Orders::where([['id', '=', $id], ['active_status', '!=', 0], ["deliveryMan_id", "=", Auth::user()->id]])->orderBy('id', 'DESC')->get();
        } else if ($user_role_id == 3) {
            $runningOrderList = Orders::where([['id', '=', $id], ["customer_id", "=", Auth::user()->id]])->orderBy('id', 'DESC')->get();
        } else if ($user_role_id == 1) {
            $runningOrderList = Orders::where([['id', '=', $id]])->orderBy('id', 'DESC')->get();
        }

        if (count($runningOrderList) < 0) {
            dd("404 Unknow request");
        }

        $order = $runningOrderList[0];
        $orderActionStatus = OrderActionStatus::where([['order_id', '=', $order->id]])->orderBy('id', 'ASC')->get();
        $id = $order->id;


        return view('order.map', compact('orderActionStatus'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function assignOrderReceived(Request $request)
    {

        $id = (empty($request->receive_id) || is_null($request->receive_id)) ? 0 : $request->receive_id;
        $lat = (empty($request->receive_lat) || is_null($request->receive_lat)) ? 0 : $request->receive_lat;
        $lag = (empty($request->receive_lag) || is_null($request->receive_lag)) ? 0 : $request->receive_lag;

        $runningOrderList = Orders::where([['id', '=', $id], ['active_status', '=', 1], ['order_status', '!=', 0], ["deliveryMan_id", "=", Auth::user()->id]])->orderBy('id', 'DESC')->get();

        if (count($runningOrderList) < 0) {
            dd("404 Unknow request");
        }

        $runningOrderList[0]->order_status = 2;
        $runningOrderList[0]->save();

        $orderStatus = new OrderActionStatus();
        $orderStatus->details = "Product receive from customer.";
        $orderStatus->track_location = "Dhaka";
        $orderStatus->order_id = $runningOrderList[0]->id;
        $orderStatus->user_id = Auth::user()->id;
        $orderStatus->latitude = $lat;
        $orderStatus->longitude = $lag;
        $orderStatus->save();

        return redirect()->route('order.order-details', $runningOrderList[0]->id)->with('success', 'Order status successfully update');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function assignOrderComplete(Request $request)
    {

        $id = (empty($request->order_complete_id) || is_null($request->order_complete_id)) ? 0 : $request->order_complete_id;
        $lat = (empty($request->order_complete_lat) || is_null($request->order_complete_lat)) ? 0 : $request->order_complete_lat;
        $lag = (empty($request->order_complete_lag) || is_null($request->order_complete_lag)) ? 0 : $request->order_complete_lag;

        $runningOrderList = Orders::where([['id', '=', $id], ['active_status', '=', 1], ['order_status', '!=', 0], ["deliveryMan_id", "=", Auth::user()->id]])->orderBy('id', 'DESC')->get();

        if (count($runningOrderList) < 0) {
            dd("404 Unknow request");
        }

        $runningOrderList[0]->order_status = 5;
        $runningOrderList[0]->save();

        $orderStatus = new OrderActionStatus();
        $orderStatus->details = "Product delivered.";
        $orderStatus->track_location = "Dhaka";
        $orderStatus->order_id = $runningOrderList[0]->id;
        $orderStatus->user_id = Auth::user()->id;
        $orderStatus->latitude = $lat;
        $orderStatus->longitude = $lag;
        $orderStatus->save();

        return redirect()->route('order.order-details', $runningOrderList[0]->id)->with('success', 'Order successfully delivered. Thank you.');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function assignOrderAddLocation(Request $request)
    {

        $id = (empty($request->add_location_id) || is_null($request->add_location_id)) ? 0 : $request->add_location_id;
        $lat = (empty($request->add_location_lat) || is_null($request->add_location_lat)) ? 0 : $request->add_location_lat;
        $lag = (empty($request->add_location_lag) || is_null($request->add_location_lag)) ? 0 : $request->add_location_lag;

        $runningOrderList = Orders::where([['id', '=', $id], ['active_status', '>=', 1], ['order_status', '!=', 0], ["deliveryMan_id", "=", Auth::user()->id]])->orderBy('id', 'DESC')->get();

        if (count($runningOrderList) < 0) {
            dd("404 Unknow request");
        } else if (count($runningOrderList) > 5) {
            return redirect()->route('order.order-details', $runningOrderList[0]->id)->with('success', 'Order status update limit over');
        }


        $runningOrderList[0]->order_status = 3;
        $runningOrderList[0]->save();

        $orderStatus = new OrderActionStatus();
        $orderStatus->details = "Product is on the way.";
        $orderStatus->track_location = "Dhaka";
        $orderStatus->order_id = $runningOrderList[0]->id;
        $orderStatus->user_id = Auth::user()->id;
        $orderStatus->latitude = $lat;
        $orderStatus->longitude = $lag;
        $orderStatus->save();

        return redirect()->route('order.order-details', $runningOrderList[0]->id)->with('success', 'Order status successfully update');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function orderDetails($id)
    {

        $user_role_id = Auth::user()->role_id;


        if ($user_role_id == 2) {
            $runningOrderList = Orders::where([['id', '=', $id], ['active_status', '!=', 0], ["deliveryMan_id", "=", Auth::user()->id]])->orderBy('id', 'DESC')->get();
        } else if ($user_role_id == 3) {
            $runningOrderList = Orders::where([['id', '=', $id], ["customer_id", "=", Auth::user()->id]])->orderBy('id', 'DESC')->get();
        } else if ($user_role_id == 1) {
            $runningOrderList = Orders::where([['id', '=', $id]])->orderBy('id', 'DESC')->get();
        }

        if (count($runningOrderList) < 0) {
            dd("404 Unknow request");
        }

        $order = $runningOrderList[0];
        $orderActionStatus = OrderActionStatus::where([['order_id', '=', $order->id]])->orderBy('id', 'DESC')->get();

        $id = $order->id;


        return view('order.details', compact('order', 'user_role_id', 'orderActionStatus', 'id'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function assignOrderList()
    {
        $runningOrderList = Orders::where([['active_status', '!=', 0],['order_status', '!=', 5], ["deliveryMan_id", "=", Auth::user()->id]])->orderBy('id', 'DESC')->get();
        $deliveryMans = User::where([['role_id', '=', 2], ['status', '=', 1]])->get();

        return view('deliveryMan.assignOrderList', compact('runningOrderList', 'deliveryMans'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getDeliveryOrderForAdmin()
    {

        $runningOrderList = Orders::where([['active_status', '!=', 0]])->whereIn('order_status', [0,1,2,3])->orderBy('id', 'DESC')->get();
        $deliveryMans = User::where([['role_id', '=', 2], ['status', '=', 1]])->get();

        return view('admin.requestList', compact('runningOrderList', 'deliveryMans'));
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function orderReject(Request $request)
    {

        $id = (empty($request->id) || is_null($request->id)) ? 0 : $request->id;

        $runningOrderList = Orders::where([['active_status', '!=', 0], ["id", "=", $id]])->get();

        if (count($runningOrderList) < 1) {
            return redirect()->route('admin.getDeliveryOrderForAdmin')->with('error', 'No order found');
        }

        $runningOrderList[0]->order_status = -1;
        $runningOrderList[0]->save();

        return redirect()->route('admin.getDeliveryOrderForAdmin')->with('success', 'Order successfully reject');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function assignDeliveryMan(Request $request)
    {
        // dd("--");

        $id = (empty($request->id) || is_null($request->id)) ? 0 : $request->id;
        $delivery_man_id = (empty($request->delivery_man_id) || is_null($request->delivery_man_id)) ? 0 : $request->delivery_man_id;

        $runningOrderList = Orders::where([['active_status', '!=', 0], ["id", "=", $id]])->get();
        $deliveryManList = User::where([['role_id', '=', 2], ['status', '=', 1], ['id', '=', $delivery_man_id]])->get();

        if (count($runningOrderList) < 1) {
            return redirect()->route('admin.getDeliveryOrderForAdmin')->with('error', 'No order found');
        }

        if (count($deliveryManList) < 1) {
            return redirect()->route('admin.getDeliveryOrderForAdmin')->with('error', 'No delivery man found');
        }

        $runningOrderList[0]->order_status = 1;
        $runningOrderList[0]->deliveryMan_id = $deliveryManList[0]->id;
        $runningOrderList[0]->save();

        return redirect()->route('admin.getDeliveryOrderForAdmin')->with('success', 'Order successfully accepted');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $validatedata = $request->validate([
            'product_category' => 'required',
            'product_weight' => 'required',
            'delivered_to' => 'required',
            'pickup_address' => 'required',
            'destination_address' => 'required',
            'contact_number' => 'required|max:11|min:11',
        ]);

        $order = new Orders();
        $order->customer_id = Auth::user()->id;
        $order->product_category = $request->product_category;
        $order->product_weight = $request->product_weight;
        $order->delivered_to = $request->delivered_to;
        $order->pickup_address =  $request->pickup_address;
        $order->destination_address =  $request->destination_address;
        $order->contact_number =  $request->contact_number;
        $order->created_by = Auth::user()->id;
        $order->updated_by = Auth::user()->id;


        $order->save();

        if ($order) {
            return redirect()->route('customer-order.newOrderRequest')->with('success', 'Order has been placed successfully');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        
        $order = Orders::where([['customer_id', '=', Auth::user()->id], ['id', '=', $id]])->firstOrFail();

        if($order->active_status == 0 or $order->order_status == -1 or $order->order_status == 5){
            return redirect()->route('customer-order.runnngOrder')->with('error', "You can't edit this order");
        } 
        return view('customer.updateOrder', compact('order', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validatedata = $request->validate([
            'product_category' => 'required',
            'product_weight' => 'required',
            'delivered_to' => 'required',
            'pickup_address' => 'required',
            'destination_address' => 'required',
            'contact_number' => 'required|max:11|min:11',
        ]);

        $order = Orders::where([['customer_id', '=', Auth::user()->id], ['id', '=', $id]])->first();

        //dd($order);

        if (empty($order) || ($order->active_status == 0)) {
            return redirect()->route('customer-order.runnngOrder')->with('error', 'Order is not exists');
        }

        $order->customer_id = Auth::user()->id;
        $order->product_category = $request->product_category;
        $order->product_weight = $request->product_weight;
        $order->delivered_to = $request->delivered_to;
        $order->pickup_address =  $request->pickup_address;
        $order->destination_address =  $request->destination_address;
        $order->contact_number =  $request->contact_number;
        $order->updated_by = Auth::user()->id;


        $order->save();

        if ($order) {
            return redirect()->route('customer-order.runnngOrder')->with('success', 'Order has been updated successfully');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function newOrderRequest()
    {


        return view('customer.newOrderRequest');
    }

    public function runnngOrder()
    {
        $runningOrderList = Orders::where([
            ['customer_id', '=', Auth::user()->id],
            ['active_status', "!=", 0]
        ])->whereIn('order_status', [1,2,3])->orderBy('id', 'DESC')->get();

        return view('customer.runningOrderList', compact('runningOrderList'));
    }

    public function orderHistory()
    {
        $runningOrderList = Orders::where([
            ['customer_id', '=', Auth::user()->id],
            ['order_status', '=', 5],
            ['active_status', "!=", 0]
        ])->orWhere(function ($query) {
            $query->where('customer_id', '=', Auth::user()->id)
                ->where('order_status', '=', -1)
                ->where('active_status', "!=", 0);
        })->orderBy('id', 'DESC')->get();

        return view('customer.historyOrderList', compact('runningOrderList'));
    }

    
    public function deliveryManCompleted()
    {
        $completedOrder = Orders::where([['deliveryMan_id' , '=',Auth::user()->id],['order_status', '=', 5], ['active_status', '=', 1]])->orderBy('id','DESC')->get();
        
        return view('deliveryMan.deliveryManCompleted', compact('completedOrder'));
    }

    
    public function adminOrderHistory()
    {
        $orderHistory = Orders::where('active_status', '=', 1)->whereIn('order_status', [-1,5])->orderBy('id','DESC')->get();
        
        return view('admin.adminOrderHistory', compact('orderHistory'));
    }
}
