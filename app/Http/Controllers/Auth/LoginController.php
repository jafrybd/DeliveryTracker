<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Redirect;
use Illuminate\Http\JsonResponse;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
   // protected $redirectTo = RouteServiceProvider::HOME;
    protected $redirectTo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest')->except('logout');
    }

    public function redirectTo()
    {

        
        $userDetails = User::where([['email', '=', Auth::user()->email],['status','=',1]])->first();
        
        if ($userDetails) {
            switch(Auth::user()->role_id){
                case 1:
                $this->redirectTo = '/adminDashboard';
                return $this->redirectTo;
                    break;
                case 2:
                        $this->redirectTo = '/deliveryManDashboard';
                    return $this->redirectTo;
                    break;
                case 3:
                    $this->redirectTo = '/customerDashboard';
                    return $this->redirectTo;
                    break;
                
                default:
                    $this->redirectTo = '/login';
                    return $this->redirectTo;
            }
            
         }
         else{
            $this->redirectTo = '/login';
            return $this->redirectTo;
           throw \Illuminate\Validation\ValidationException::withMessages(['No such user']);
           
         }
        
         
        // return $next($request);
    } 

    public function changePasswordUI()
    {
        
            return view('auth.passwords.change_password');
        
        
       
    }

    
    public function changePasswordConfirm(Request $request)
    {       
        $user = Auth::user();
    
        $userPassword = $user->password;
        
        $request->validate([
            'current_password' => 'required',
            'password' => 'required|same:confirm_password|min:8',
            'confirm_password' => 'required',
        ]);

        if (!Hash::check($request->current_password, $userPassword)) {
            return back()->withErrors(['current_password'=>'Current password not matched']);
        }

        $user->password = Hash::make($request->password);

        $user->save();

        if(Auth::user()->role_id == 1){
            return redirect()->route('adminDashboard')->with('success', 'Password has been updated successfully');
        } else if(Auth::user()->role_id == 2){
            return redirect()->route('deliveryManDashboard')->with('success', 'Password has been updated successfully');
        } else if (Auth::user()->role_id == 3){
            return redirect()->route('customerDashboard')->with('success', 'Password has been updated successfully');
        }

        
    }
}