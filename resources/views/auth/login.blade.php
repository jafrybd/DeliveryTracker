<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="description" content="" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title  -->
    <title>Delivery Tracker - Homepage</title>

    <!-- Favicon  -->
    <!-- <link rel="icon" href="{{ asset('../admin/assets/img/favicon.png') }}" /> -->

    <!-- ***** All CSS Files ***** -->

    <!-- Style css -->
    <link rel="stylesheet" href="{{ asset('../admin/assets/css/style.css') }}" />
</head>

<body class="homepage-5">
    <!--====== Scroll To Top Area Start ======-->
    <div id="scrollUp" title="Scroll To Top">
        <i class="fas fa-arrow-up"></i>
    </div>
    <!--====== Scroll To Top Area End ======-->

    <div class="main">
        <!-- ***** Header Start ***** -->
        <header class="navbar navbar-sticky navbar-expand-lg navbar-dark">
            <div class="container position-relative">
                <a class="navbar-brand" href="index.html">
                    <!-- <img
              class="navbar-brand-regular"
              src="#"
            /> -->
                    <img class="navbar-brand-sticky" src="{{ asset('../admin/img2//logo.png') }}" alt="sticky brand-logo" width="250px" />
                </a>
                <button class="navbar-toggler d-lg-none" type="button" data-toggle="navbarToggler" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="navbar-inner">
                    <!--  Mobile Menu Toggler -->
                    <button class="navbar-toggler d-lg-none" type="button" data-toggle="navbarToggler" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <nav>
                        <ul class="navbar-nav" id="navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link scroll" href="#home" id="navbarDropdownMenuLink">
                                    Home
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link scroll" href="#team">Team</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link scroll" href="#hiw">How its work</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link scroll" href="#pricing">Pricing</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link scroll" href="#contact">Contact</a>
                            </li>
                            <li class="nav-item registerBtn">
                                @if (Route::has('register'))
                                <a class="" href="{{ route('register') }}">Create New Account</a>
                                @endif
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </header>
        <!-- ***** Header End ***** -->

        <!-- ***** Welcome Area Start ***** -->
        <section id="home" class="section welcome-area d-flex align-items-center">
            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <!-- Welcome Intro Start -->

                    <div class="col-12 col-md-8 col-lg-5">
                        <!-- Contact Box -->
                        <div class="
                  contact-box
                  bg-white
                  text-center
                  rounded
                  p-4 p-sm-5
                  mt-5 mt-lg-0
                  shadow-lg
                ">
                            <!-- Contact Form -->

                            <div class="contact-top">
                                <h3 class="contact-title">Signin Here!</h3>
                            </div>
                            @if ($errors->any())
                            <div class="col-sm-12">
                                <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                                    @foreach ($errors->all() as $error)
                                    <span>
                                        <p>{{ $error }}</p>
                                    </span>
                                    @endforeach
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>
                            @endif

                            @include('flash-message')

                            <div class="row justify-content-center  mt-5">
                                <form method="POST" action="{{ route('login') }}">
                                    @csrf
                                    <div class="col-12">
                                        <div class="form-group">
                                            <input id="email" type="email" class="form-control input @error('email') is-invalid @enderror" value="{{ old('email') }}" name="email" placeholder="Enter Email" required autocomplete="off" autofocus />
                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <input id="password" type="password" class="form-control input @error('password') is-invalid @enderror" placeholder="Enter Password" name="password" required autocomplete="current-password" />
                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <button class="btn btn-bordered w-100 mt-3 mt-sm-4" type="submit">
                                            {{ __('Login') }}
                                        </button>
                                        <div class="contact-bottom row">
                                            <!-- @if (Route::has('register'))
                                                <a class="btn btn-dark" href="{{ route('register') }}">Create New Account</a>
                                                @endif -->

                                            @if (Route::has('password.request'))
                                            <a class="mt-2 col-12 text-right" href="{{ route('password.request') }}">
                                                Forgot Password
                                            </a>
                                            @endif
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <p class="form-message"></p>
                        </div>
                    </div>
                    <div class="col-12 col-lg-7">
                        <div class="welcome-intro">
                            <!-- <h1 class="text-white">Stay connected with your customers</h1>
                            <p class="text-white my-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit nihil tenetur minus quidem est deserunt molestias accusamus harum ullam tempore debitis et, expedita, repellat delectus aspernatur neque itaque qui quod.</p>
                            <div class="button-group store-buttons d-flex">
                                <a href="#" class="btn sApp-btn text-uppercase">Start free trail</a>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- Shape Bottom -->
            <div class="shape-bottom">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none">
                    <path class="shape-fill" fill="#FFFFFF" d="M421.9,6.5c22.6-2.5,51.5,0.4,75.5,5.3c23.6,4.9,70.9,23.5,100.5,35.7c75.8,32.2,133.7,44.5,192.6,49.7  c23.6,2.1,48.7,3.5,103.4-2.5c54.7-6,106.2-25.6,106.2-25.6V0H0v30.3c0,0,72,32.6,158.4,30.5c39.2-0.7,92.8-6.7,134-22.4  c21.2-8.1,52.2-18.2,79.7-24.2C399.3,7.9,411.6,7.5,421.9,6.5z"></path>
                </svg>
            </div>
        </section>
        <!-- ***** Welcome Area End ***** -->

        <!-- ***** Team Area Start ***** -->
        <section id="team" class="section team-area team-style-two overflow-hidden ptb_100">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-10 col-lg-8">
                        <!-- Section Heading -->
                        <div class="section-heading text-center">
                            <h2 class="text-capitalize">Our Team Experts</h2>
                            <p class="d-none d-sm-block mt-4">
                                Team member behind this project
                            </p>
                            <p class="d-block d-sm-none mt-4">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                Laborum obcaecati.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-12 col-sm-6 col-md-4 col-lg-4">
                        <!-- Single Team -->
                        <div class="single-team text-center overflow-hidden m-2 m-lg-0">
                            <!-- Team Thumb -->
                            <div class="
                    team-thumb
                    d-inline-block
                    position-relative
                    overflow-hidden
                  ">
                                <img src="{{ asset('../admin/img2/tom.jpg') }}" alt="" />
                                <!-- Team Overlay -->
                                <div class="team-overlay hh">
                                    <h4 class="team-name text-white">Rion Sarker Tonmoy</h4>
                                    <h6 class="team-post text-white mt-2 mb-3">
                                        ID: 18.02.08.018
                                    </h6>
                                    <!-- Team Icons -->
                                    <div class="team-icons">
                                        <a class="p-2" href="#"><i class="fab fa-facebook-f"></i></a>
                                        <a class="p-2" href="#"><i class="fab fa-twitter"></i></a>
                                        <a class="p-2" href="#"><i class="fab fa-google-plus-g"></i></a>
                                        <a class="p-2" href="#"><i class="fab fa-linkedin-in"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-4">
                        <!-- Single Team -->
                        <div class="single-team text-center overflow-hidden m-2 m-lg-0">
                            <!-- Team Thumb -->
                            <div class="
                    team-thumb
                    d-inline-block
                    position-relative
                    overflow-hidden
                  ">
                                <img src="{{ asset('../admin/img2/akib.jpg') }}" alt="" />
                                <!-- Team Overlay -->
                                <div class="team-overlay hh2">
                                    <h4 class="team-name text-white">Akib Mohammed</h4>
                                    <h6 class="team-post text-white mt-2 mb-3">
                                        ID: 18.02.08.045
                                    </h6>
                                    <!-- Team Icons -->
                                    <div class="team-icons">
                                        <a class="p-2" href="#"><i class="fab fa-facebook-f"></i></a>
                                        <a class="p-2" href="#"><i class="fab fa-twitter"></i></a>
                                        <a class="p-2" href="#"><i class="fab fa-google-plus-g"></i></a>
                                        <a class="p-2" href="#"><i class="fab fa-linkedin-in"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-4">
                        <!-- Single Team -->
                        <div class="single-team text-center overflow-hidden m-2 m-lg-0">
                            <!-- Team Thumb -->
                            <div class="
                    team-thumb
                    d-inline-block
                    position-relative
                    overflow-hidden
                  ">
                                <img src="{{ asset('../admin/img2/asif.jpg') }}" alt="" />
                                <!-- Team Overlay -->
                                <div class="team-overlay hh2">
                                    <h4 class="team-name text-white">Asif Zaman</h4>
                                    <h6 class="team-post text-white mt-2 mb-3">
                                        ID: 15.02.08.058
                                    </h6>
                                    <!-- Team Icons -->
                                    <div class="team-icons">
                                        <a class="p-2" href="#"><i class="fab fa-facebook-f"></i></a>
                                        <a class="p-2" href="#"><i class="fab fa-twitter"></i></a>
                                        <a class="p-2" href="#"><i class="fab fa-google-plus-g"></i></a>
                                        <a class="p-2" href="#"><i class="fab fa-linkedin-in"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-3 col-lg-4 mt-5">
                        <!-- Single Team -->
                        <div class="single-team text-center overflow-hidden m-2 m-lg-0">
                            <!-- Team Thumb -->
                            <div class="
                    team-thumb
                    d-inline-block
                    position-relative
                    overflow-hidden
                  ">
                                <img src="{{ asset('../admin/img2/rrr.jpeg') }}" alt="" />
                                <!-- Team Overlay -->
                                <div class="team-overlay hh2">
                                    <h4 class="team-name text-white">
                                        Syed Junayed Ahmed Niloy
                                    </h4>
                                    <h6 class="team-post text-white mt-2 mb-3">
                                        ID: 18.02.08.070
                                    </h6>
                                    <!-- Team Icons -->
                                    <div class="team-icons">
                                        <a class="p-2" href="#"><i class="fab fa-facebook-f"></i></a>
                                        <a class="p-2" href="#"><i class="fab fa-twitter"></i></a>
                                        <a class="p-2" href="#"><i class="fab fa-google-plus-g"></i></a>
                                        <a class="p-2" href="#"><i class="fab fa-linkedin-in"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-4 mt-5">
                        <!-- Single Team -->
                        <div class="single-team text-center overflow-hidden m-2 m-lg-0">
                            <!-- Team Thumb -->
                            <div class="
                    team-thumb
                    d-inline-block
                    position-relative
                    overflow-hidden
                  ">
                                <img src="{{ asset('../admin/img2/fifth.jpg') }}" alt="" />
                                <!-- Team Overlay -->
                                <div class="team-overlay">
                                    <h4 class="team-name text-white">Bishal Saha</h4>
                                    <h6 class="team-post text-white mt-2 mb-3">
                                        ID: 18.02.08.023
                                    </h6>
                                    <!-- Team Icons -->
                                    <div class="team-icons">
                                        <a class="p-2" href="#"><i class="fab fa-facebook-f"></i></a>
                                        <a class="p-2" href="#"><i class="fab fa-twitter"></i></a>
                                        <a class="p-2" href="#"><i class="fab fa-google-plus-g"></i></a>
                                        <a class="p-2" href="#"><i class="fab fa-linkedin-in"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- ***** Team Area End ***** -->

        <!-- ***** Work Area Start ***** -->
        <section id="hiw" class="section work-area bg-overlay overflow-hidden ptb_100">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-10 col-lg-8">
                        <!-- Work Content -->
                        <div class="work-content text-center">
                            <h2 class="text-white">How it's works?</h2>
                        </div>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-12 col-md-4">
                        <!-- Single Work -->
                        <div class="single-work text-center p-3">
                            <!-- Work Icon -->
                            <div class="work-icon">
                                <img class="avatar-md" src="{{ asset('../admin/assets/img/icon/work/download.png') }}" alt="" />
                            </div>
                            <h3 class="text-white py-3">Login</h3>
                            <p class="text-white">
                                To access our website, customer needs to access through login
                                form. To get the login credentials please register your
                                account.
                            </p>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <!-- Single Work -->
                        <div class="single-work text-center p-3">
                            <!-- Work Icon -->
                            <div class="work-icon">
                                <img class="avatar-md" src="{{ asset('../admin/assets/img/icon/work/settings.png') }}" alt="" />
                            </div>
                            <h3 class="text-white py-3">Request Order Delivery</h3>
                            <p class="text-white">
                                Request a order from New Order page & after submit a request
                                will sent to admin. If Admin accept the request then you can
                                check your order status.
                            </p>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <!-- Single Work -->
                        <div class="single-work text-center p-3">
                            <!-- Work Icon -->
                            <div class="work-icon">
                                <img class="avatar-md" src="{{ asset('../admin/assets/img/icon/work/app.png') }}" alt="" />
                            </div>
                            <h3 class="text-white py-3">Track Delivery</h3>
                            <p class="text-white">
                                Every time assigned delivery man change your order
                                location/status, you'll be able to see and track your order
                                location.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- ***** Work Area End ***** -->
        <section id="review" class="review-area ptb_100">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-10 col-lg-8">
                        <!-- Section Heading -->
                        <div class="section-heading text-center">
                            <span class="d-inline-block rounded-pill shadow-sm fw-5 px-4 py-2 mb-3">
                                <svg class="svg-inline--fa fa-lightbulb fa-w-11 text-primary mr-1" aria-hidden="true" focusable="false" data-prefix="far" data-icon="lightbulb" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512" data-fa-i2svg="">
                                    <path fill="currentColor" d="M176 80c-52.94 0-96 43.06-96 96 0 8.84 7.16 16 16 16s16-7.16 16-16c0-35.3 28.72-64 64-64 8.84 0 16-7.16 16-16s-7.16-16-16-16zM96.06 459.17c0 3.15.93 6.22 2.68 8.84l24.51 36.84c2.97 4.46 7.97 7.14 13.32 7.14h78.85c5.36 0 10.36-2.68 13.32-7.14l24.51-36.84c1.74-2.62 2.67-5.7 2.68-8.84l.05-43.18H96.02l.04 43.18zM176 0C73.72 0 0 82.97 0 176c0 44.37 16.45 84.85 43.56 115.78 16.64 18.99 42.74 58.8 52.42 92.16v.06h48v-.12c-.01-4.77-.72-9.51-2.15-14.07-5.59-17.81-22.82-64.77-62.17-109.67-20.54-23.43-31.52-53.15-31.61-84.14-.2-73.64 59.67-128 127.95-128 70.58 0 128 57.42 128 128 0 30.97-11.24 60.85-31.65 84.14-39.11 44.61-56.42 91.47-62.1 109.46a47.507 47.507 0 0 0-2.22 14.3v.1h48v-.05c9.68-33.37 35.78-73.18 52.42-92.16C335.55 260.85 352 220.37 352 176 352 78.8 273.2 0 176 0z"></path>
                                </svg><!-- <i class="far fa-lightbulb text-primary mr-1"></i> -->
                                <span class="text-primary">Customer's</span>
                                Reviews
                            </span>
                            <h2 class="text-capitalize">What our customers are saying</h2>
                            <p class="d-none d-sm-block mt-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum obcaecati dignissimos quae quo ad iste ipsum officiis deleniti asperiores sit.</p>
                            <p class="d-block d-sm-none mt-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum obcaecati.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-4 res-margin">
                        <!-- Single Review -->
                        <div class="single-review card">
                            <!-- Card Top -->
                            <div class="card-top p-4">
                                <div class="review-icon">
                                    <svg class="svg-inline--fa fa-star fa-w-18 text-warning" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg="">
                                        <path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path>
                                    </svg><!-- <i class="fas fa-star text-warning"></i> -->
                                    <svg class="svg-inline--fa fa-star fa-w-18 text-warning" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg="">
                                        <path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path>
                                    </svg><!-- <i class="fas fa-star text-warning"></i> -->
                                    <svg class="svg-inline--fa fa-star fa-w-18 text-warning" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg="">
                                        <path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path>
                                    </svg><!-- <i class="fas fa-star text-warning"></i> -->
                                    <svg class="svg-inline--fa fa-star fa-w-18 text-warning" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg="">
                                        <path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path>
                                    </svg><!-- <i class="fas fa-star text-warning"></i> -->
                                    <svg class="svg-inline--fa fa-star fa-w-18 text-warning" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg="">
                                        <path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path>
                                    </svg><!-- <i class="fas fa-star text-warning"></i> -->
                                </div>
                                <h4 class="text-primary mt-4 mb-3">Excellent service &amp; support!!</h4>
                                <!-- Review Text -->
                                <div class="review-text">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis nam id facilis, provident doloremque placeat eveniet molestias laboriosam. Optio, esse.</p>
                                </div>
                                <!-- Quotation Icon -->
                                <div class="quot-icon">
                                    <img class="avatar-md" src="{{ asset('../admin/assets/img/icon/quote.png') }}" alt="">
                                </div>
                            </div>
                            <!-- Reviewer -->
                            <div class="reviewer media bg-gray p-4">
                                <!-- Reviewer Thumb -->
                                <div class="reviewer-thumb">
                                    <img class="avatar-lg radius-100" src="{{ asset('../admin/assets/img/avatar/avatar-1.png') }}" alt="">
                                </div>
                                <!-- Reviewer Media -->
                                <div class="reviewer-meta media-body align-self-center ml-4">
                                    <h5 class="reviewer-name color-primary mb-2">Demo User</h5>
                                    <h6 class="text-secondary fw-6">CEO, Dummy Company</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4 res-margin">
                        <!-- Single Review -->
                        <div class="single-review card">
                            <!-- Card Top -->
                            <div class="card-top p-4">
                                <div class="review-icon">
                                    <svg class="svg-inline--fa fa-star fa-w-18 text-warning" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg="">
                                        <path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path>
                                    </svg><!-- <i class="fas fa-star text-warning"></i> -->
                                    <svg class="svg-inline--fa fa-star fa-w-18 text-warning" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg="">
                                        <path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path>
                                    </svg><!-- <i class="fas fa-star text-warning"></i> -->
                                    <svg class="svg-inline--fa fa-star fa-w-18 text-warning" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg="">
                                        <path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path>
                                    </svg><!-- <i class="fas fa-star text-warning"></i> -->
                                    <svg class="svg-inline--fa fa-star fa-w-18 text-warning" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg="">
                                        <path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path>
                                    </svg><!-- <i class="fas fa-star text-warning"></i> -->
                                    <svg class="svg-inline--fa fa-star-half-alt fa-w-17 text-warning" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star-half-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 536 512" data-fa-i2svg="">
                                        <path fill="currentColor" d="M508.55 171.51L362.18 150.2 296.77 17.81C290.89 5.98 279.42 0 267.95 0c-11.4 0-22.79 5.9-28.69 17.81l-65.43 132.38-146.38 21.29c-26.25 3.8-36.77 36.09-17.74 54.59l105.89 103-25.06 145.48C86.98 495.33 103.57 512 122.15 512c4.93 0 10-1.17 14.87-3.75l130.95-68.68 130.94 68.7c4.86 2.55 9.92 3.71 14.83 3.71 18.6 0 35.22-16.61 31.66-37.4l-25.03-145.49 105.91-102.98c19.04-18.5 8.52-50.8-17.73-54.6zm-121.74 123.2l-18.12 17.62 4.28 24.88 19.52 113.45-102.13-53.59-22.38-11.74.03-317.19 51.03 103.29 11.18 22.63 25.01 3.64 114.23 16.63-82.65 80.38z"></path>
                                    </svg><!-- <i class="fas fa-star-half-alt text-warning"></i> -->
                                </div>
                                <h4 class="text-primary mt-4 mb-3">Nice work! Keep it up</h4>
                                <!-- Review Text -->
                                <div class="review-text">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis nam id facilis, provident doloremque placeat eveniet molestias laboriosam. Optio, esse.</p>
                                </div>
                                <!-- Quotation Icon -->
                                <div class="quot-icon">
                                    <img class="avatar-md" src="{{ asset('../admin/assets/img/icon/quote.png') }}" alt="">
                                </div>
                            </div>
                            <!-- Reviewer -->
                            <div class="reviewer media bg-gray p-4">
                                <!-- Reviewer Thumb -->
                                <div class="reviewer-thumb">
                                    <img class="avatar-lg radius-100" src="{{ asset('../admin/assets/img/avatar/avatar-1.png') }}" alt="">
                                </div>
                                <!-- Reviewer Media -->
                                <div class="reviewer-meta media-body align-self-center ml-4">
                                    <h5 class="reviewer-name color-primary mb-2">Demo User</h5>
                                    <h6 class="text-secondary fw-6">CEO, Dummy Company</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4">
                        <!-- Single Review -->
                        <div class="single-review card">
                            <!-- Card Top -->
                            <div class="card-top p-4">
                                <div class="review-icon">
                                    <svg class="svg-inline--fa fa-star fa-w-18 text-warning" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg="">
                                        <path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path>
                                    </svg><!-- <i class="fas fa-star text-warning"></i> -->
                                    <svg class="svg-inline--fa fa-star fa-w-18 text-warning" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg="">
                                        <path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path>
                                    </svg><!-- <i class="fas fa-star text-warning"></i> -->
                                    <svg class="svg-inline--fa fa-star fa-w-18 text-warning" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg="">
                                        <path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path>
                                    </svg><!-- <i class="fas fa-star text-warning"></i> -->
                                    <svg class="svg-inline--fa fa-star fa-w-18 text-warning" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg="">
                                        <path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path>
                                    </svg><!-- <i class="fas fa-star text-warning"></i> -->
                                    <svg class="svg-inline--fa fa-star fa-w-18 text-warning" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg="">
                                        <path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path>
                                    </svg><!-- <i class="fas fa-star text-warning"></i> -->
                                </div>
                                <h4 class="text-primary mt-4 mb-3">Great support!!</h4>
                                <!-- Review Text -->
                                <div class="review-text">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis nam id facilis, provident doloremque placeat eveniet molestias laboriosam. Optio, esse.</p>
                                </div>
                                <!-- Quotation Icon -->
                                <div class="quot-icon">
                                    <img class="avatar-md" src="{{ asset('../admin/assets/img/icon/quote.png') }}" alt="">
                                </div>
                            </div>
                            <!-- Reviewer -->
                            <div class="reviewer media bg-gray p-4">
                                <!-- Reviewer Thumb -->
                                <div class="reviewer-thumb">
                                    <img class="avatar-lg radius-100" src="{{ asset('../admin/assets/img/avatar/avatar-1.png') }}" alt="">
                                </div>
                                <!-- Reviewer Media -->
                                <div class="reviewer-meta media-body align-self-center ml-4">
                                    <h5 class="reviewer-name color-primary mb-2">Demo User</h5>
                                    <h6 class="text-secondary fw-6">CEO, Dummy Company</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- ***** Price Plan Area Start ***** -->
        <section id="pricing" class="section price-plan-area bg-gray overflow-hidden ptb_100">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-12 col-lg-12">
                        <!-- Section Heading -->
                        <div class="section-heading text-center">
                            <h2>Shipment Price</h2>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-12 col-sm-10 col-lg-12">
                        <div class="row price-plan-wrapper">
                            <div class="col-12 col-md-4">
                                <!-- Single Price Plan -->
                                <div class="single-price-plan text-center p-5 wow fadeInLeft" data-aos-duration="2s" data-wow-delay="0.4s">
                                    <!-- Plan Thumb -->
                                    <div class="plan-thumb">
                                        <img class="avatar-lg" src="{{ asset('../admin/assets/img/pricing/basic.png') }}" alt="" />
                                    </div>
                                    <!-- Plan Title -->
                                    <div class="plan-title my-2 my-sm-3">
                                        <h4 class="text-uppercase">Frozen Products</h4>
                                    </div>
                                    <!-- Plan Price -->
                                    <div class="plan-price pb-2 pb-sm-3">
                                        <h2 class="fw-7"><small class="fw-6">BDT</small>800</h2>
                                    </div>
                                    <!-- Plan Description -->
                                    <div class="plan-description">
                                        <ul class="plan-features">
                                            <li class="border-top py-3">Distance 100km</li>
                                            <li class="border-top py-3">Weight 15kg</li>
                                            <li class="border-top py-3">Reefer Containers Available</li>
                                            <li class="border-top py-3">Live Tracking Updates Available</li>
                                        </ul>
                                    </div>
                                    <!-- Plan Button -->
                                    <div class="plan-button">
                                        <a href="#" class="btn mt-4">Purchase</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-4 mt-4 mt-md-0">
                                <!-- Single Price Plan -->
                                <div class="single-price-plan text-center p-5 wow fadeInRight" data-aos-duration="2s" data-wow-delay="0.4s">
                                    <!-- Plan Thumb -->
                                    <div class="plan-thumb">
                                        <img class="avatar-lg" src="{{ asset('../admin/assets/img/pricing/premium.png') }}" alt="" />
                                    </div>
                                    <!-- Plan Title -->
                                    <div class="plan-title my-2 my-sm-3">
                                        <h4 class="text-uppercase">Foods & Drinking Products </h4>
                                    </div>
                                    <!-- Plan Price -->
                                    <div class="plan-price pb-2 pb-sm-3">
                                        <h2 class="fw-7"><small class="fw-6">BDT</small>500</h2>
                                    </div>
                                    <!-- Plan Description -->
                                    <div class="plan-description">
                                        <ul class="plan-features">
                                            <li class="border-top py-3">Distance 100km</li>
                                            <li class="border-top py-3">Weight 10kg</li>
                                            <li class="border-top py-3">Reefer Containers Available</li>
                                            <li class="border-top border-bottom py-3">
                                                Live Tracking Updates Available
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- Plan Button -->
                                    <div class="plan-button">
                                        <a href="#" class="btn mt-4">Purchase</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-4 mt-4 mt-md-0">
                                <!-- Single Price Plan -->
                                <div class="single-price-plan text-center p-5 wow fadeInRight" data-aos-duration="2s" data-wow-delay="0.4s">
                                    <!-- Plan Thumb -->
                                    <div class="plan-thumb">
                                        <img class="avatar-lg" src="{{ asset('../admin/assets/img/pricing/premium.png') }}" alt="" />
                                    </div>
                                    <!-- Plan Title -->
                                    <div class="plan-title my-2 my-sm-3">
                                        <h4 class="text-uppercase">Furniture, Electronic Goods</h4>
                                    </div>
                                    <!-- Plan Price -->
                                    <div class="plan-price pb-2 pb-sm-3">
                                        <h2 class="fw-7"><small class="fw-6">BDT</small>3000</h2>
                                    </div>
                                    <!-- Plan Description -->
                                    <div class="plan-description">
                                        <ul class="plan-features">
                                            <li class="border-top py-3">Distance 100km</li>
                                            <li class="border-top py-3">Weight Heavy Goods</li>
                                            <li class="border-top py-3">Maximum security for goods</li>
                                            <!-- <li class="border-top border-bottom py-3">
                                                Live Tracking Updates Available
                                            </li> -->
                                        </ul>
                                    </div>
                                    <!-- Plan Button -->
                                    <div class="plan-button">
                                        <a href="#" class="btn mt-4">Purchase</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <!-- FAQ Content -->
                        <div class="faq-content">
                            <span class="d-block text-center mt-5">Not sure what to choose? <a href="#">Contact Us</a></span>
                            <!-- sApp Accordion -->
                            <div class="accordion pt-5" id="apolo-accordion">
                                <div class="row">
                                    <div class="col-12 col-lg-6">
                                        <!-- Single Accordion Item -->
                                        <div class="card my-2">
                                            <!-- Card Header -->
                                            <div class="card-header bg-white">
                                                <h2 class="mb-0">
                                                    <button class="btn p-2" type="button" data-toggle="collapse" data-target="#collapseOne">
                                                        How to Login/Signin?
                                                    </button>
                                                </h2>
                                            </div>
                                            <div id="collapseOne" class="collapse show" data-parent="#apolo-accordion">
                                                <!-- Card Body -->
                                                <div class="card-body">
                                                    In homepage you'll be able to find a Signin Form
                                                    from where you can give your email & password. Only
                                                    Authenticated users can login.
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Single Accordion Item -->
                                        <div class="card my-2">
                                            <!-- Card Header -->
                                            <div class="card-header bg-white">
                                                <h2 class="mb-0">
                                                    <button class="btn collapsed p-2" type="button" data-toggle="collapse" data-target="#collapseTwo">
                                                        How to Register/Signup here?
                                                    </button>
                                                </h2>
                                            </div>
                                            <div id="collapseTwo" class="collapse" data-parent="#apolo-accordion">
                                                <!-- Card Body -->
                                                <div class="card-body">
                                                    From Register page, you'll be able to register or
                                                    signup a new account. You can access the Register
                                                    page through register button.
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Single Accordion Item -->
                                        <div class="card my-2">
                                            <!-- Card Header -->
                                            <div class="card-header bg-white">
                                                <h2 class="mb-0">
                                                    <button class="btn collapsed p-2" type="button" data-toggle="collapse" data-target="#collapseThree">
                                                        Contact form isn't working?
                                                    </button>
                                                </h2>
                                            </div>
                                            <div id="collapseThree" class="collapse" data-parent="#apolo-accordion">
                                                <!-- Card Body -->
                                                <div class="card-body">
                                                    Anim pariatur cliche reprehenderit, enim eiusmod
                                                    high life accusamus terry richardson ad squid. 3
                                                    wolf moon officia aute, non cupidatat skateboard
                                                    dolor brunch. Food truck quinoa.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-6">
                                        <!-- Single Accordion Item -->
                                        <div class="card my-2">
                                            <!-- Card Header -->
                                            <div class="card-header bg-white">
                                                <h2 class="mb-0">
                                                    <button class="btn collapsed p-2" type="button" data-toggle="collapse" data-target="#collapseFour">
                                                        What about the events?
                                                    </button>
                                                </h2>
                                            </div>
                                            <div id="collapseFour" class="collapse" data-parent="#apolo-accordion">
                                                <!-- Card Body -->
                                                <div class="card-body">
                                                    Anim pariatur cliche reprehenderit, enim eiusmod
                                                    high life accusamus terry richardson ad squid. 3
                                                    wolf moon officia aute, non cupidatat skateboard
                                                    dolor brunch. Food truck quinoa.
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Single Accordion Item -->
                                        <div class="card my-2">
                                            <!-- Card Header -->
                                            <div class="card-header bg-white">
                                                <h2 class="mb-0">
                                                    <button class="btn p-2" type="button" data-toggle="collapse" data-target="#collapseFive">
                                                        How can I get product update?
                                                    </button>
                                                </h2>
                                            </div>
                                            <div id="collapseFive" class="collapse show" data-parent="#apolo-accordion">
                                                <!-- Card Body -->
                                                <div class="card-body">
                                                    Anim pariatur cliche reprehenderit, enim eiusmod
                                                    high life accusamus terry richardson ad squid. 3
                                                    wolf moon officia aute, non cupidatat skateboard
                                                    dolor brunch. Food truck quinoa.
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Single Accordion Item -->
                                        <div class="card my-2">
                                            <!-- Card Header -->
                                            <div class="card-header bg-white">
                                                <h2 class="mb-0">
                                                    <button class="btn collapsed p-2" type="button" data-toggle="collapse" data-target="#collapseSix">
                                                        Is this template support rtl?
                                                    </button>
                                                </h2>
                                            </div>
                                            <div id="collapseSix" class="collapse" data-parent="#apolo-accordion">
                                                <!-- Card Body -->
                                                <div class="card-body">
                                                    Anim pariatur cliche reprehenderit, enim eiusmod
                                                    high life accusamus terry richardson ad squid. 3
                                                    wolf moon officia aute, non cupidatat skateboard
                                                    dolor brunch. Food truck quinoa.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- ***** Price Plan Area End ***** -->

        <!-- ***** Subscribe Area Start ***** -->
        <section class="section subscribe-area ptb_100">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-10 col-lg-8">
                        <div class="subscribe-content text-center">
                            <h2>Subscribe to get updates</h2>
                            <p class="my-4">
                                By subscribing you will get newsleter, promotions adipisicing
                                elit. Architecto beatae, asperiores tempore repudiandae saepe
                                aspernatur unde voluptate sapiente quia ex.
                            </p>
                            <!-- Subscribe Form -->
                            <form class="subscribe-form">
                                <div class="form-group">
                                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter your email" />
                                </div>
                                <button type="submit" class="btn btn-lg btn-block">
                                    Subscribe
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- ***** Subscribe Area End ***** -->

        <!--====== Contact Area Start ======-->
        <section id="contact" class="contact-area bg-gray ptb_100">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-10 col-lg-8">
                        <!-- Section Heading -->
                        <div class="section-heading text-center">
                            <h2 class="text-capitalize">Stay Tuned</h2>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-between">
                    <div class="col-12 col-md-5">
                        <!-- Contact Us -->
                        <div class="contact-us">
                            <p class="mb-3">
                                The Ahsanullah University of Science and Technology (AUST) was founded by the Dhaka Ahsania Mission in 1995. Dhaka Ahsania Mission is a non-profit voluntary organization in Bangladesh. The Mission was established in 1958 by Khan Bahadur Ahsanullah, an outstanding educationist and social reformer of undivided India.
                            </p>
                            <ul>
                                <li class="py-2">
                                    <a class="media" href="#">
                                        <div class="social-icon mr-3">
                                            <i class="fas fa-home"></i>
                                        </div>
                                        <span class="media-body align-self-center">141 & 142, Love Road, Tejgaon Industrial Area, Dhaka-1208.</span>
                                    </a>
                                </li>
                                <li class="py-2">
                                    <a class="media" href="#">
                                        <div class="social-icon mr-3">
                                            <i class="fas fa-phone-alt"></i>
                                        </div>
                                        <span class="media-body align-self-center">Tel. (8802) 8870422, Ext. 107, 114, Fax : (8802) 8870417-18</span>
                                    </a>
                                </li>
                                <li class="py-2">
                                    <a class="media" href="#">
                                        <div class="social-icon mr-3">
                                            <i class="fas fa-envelope"></i>
                                        </div>
                                        <span class="media-body align-self-center"> info@aust.edu, regr@aust.edu</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 pt-4 pt-md-0">
                        <!-- Contact Box -->
                        <div class="contact-box text-center">
                            <!-- Contact Form -->
                            <form id="contact-form" method="POST" action="assets/php/mail.php">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="name" placeholder="Name" required="required" />
                                        </div>
                                        <div class="form-group">
                                            <input type="email" class="form-control" name="email" placeholder="Email" required="required" />
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="subject" placeholder="Subject" required="required" />
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <textarea class="form-control" name="message" placeholder="Message" required="required"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <button type="submit" class="btn btn-lg btn-block mt-3">
                                            <span class="text-white pr-3"><i class="fas fa-paper-plane"></i></span>Send Message
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <p class="form-message"></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--====== Contact Area End ======-->
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <!-- Copyright Area -->
                        <div class="copyright-area d-flex flex-wrap justify-content-center justify-content-sm-between text-center py-4">
                            <!-- Copyright Left -->
                            <div class="copyright-left"><img src="https://www.aust.edu/images/aust_logo.svg" width="40px" /> Ahsanullah University of Science and Technology.</div>
                            <!-- Copyright Right -->
                            <div class="copyright-right">Made with <svg class="svg-inline--fa fa-heart fa-w-16" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="heart" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
                                    <path fill="currentColor" d="M462.3 62.6C407.5 15.9 326 24.3 275.7 76.2L256 96.5l-19.7-20.3C186.1 24.3 104.5 15.9 49.7 62.6c-62.8 53.6-66.1 149.8-9.9 207.9l193.5 199.8c12.5 12.9 32.8 12.9 45.3 0l193.5-199.8c56.3-58.1 53-154.3-9.8-207.9z"></path>
                                </svg><!-- <i class="fas fa-heart"></i> --> By <a href="#team">TEAM</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ***** All jQuery Plugins ***** -->
    <script src="{{ asset('../admin/js/loginForm.js') }}"></script>

    <!-- jQuery(necessary for all JavaScript plugins) -->
    <script src="{{ asset('../admin/assets/js/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap js -->
    <script src="{{ asset('../admin/assets/js/bootstrap/popper.min.js') }}"></script>
    <script src="{{ asset('../admin/assets/js/bootstrap/bootstrap.min.js') }}"></script>

    <!-- Plugins js -->
    <script src="{{ asset('../admin/assets/js/plugins/plugins.min.js') }}"></script>

    <!-- Active js -->
    <script src="{{ asset('../admin/assets/js/active.js') }}"></script>
</body>

</html>