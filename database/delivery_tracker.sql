-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 28, 2021 at 05:14 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `delivery_tracker`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_08_19_021339_create_orders_table', 2),
(5, '2021_08_19_135826_create_order_action_status', 3);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `deliveryMan_id` bigint(20) UNSIGNED DEFAULT NULL,
  `product_category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_weight` int(11) NOT NULL,
  `delivered_to` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pickup_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `destination_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` double(13,2) NOT NULL DEFAULT 0.00,
  `order_status` int(11) NOT NULL DEFAULT 0,
  `active_status` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `customer_id`, `deliveryMan_id`, `product_category`, `product_weight`, `delivered_to`, `pickup_address`, `destination_address`, `contact_number`, `amount`, `order_status`, `active_status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 7, 16, 'Gadget', 20, 'Rakib', 'Dhaka', 'Cumitlla', '01678678678', 0.00, 5, 1, 7, 7, '2021-08-18 20:47:21', '2021-08-27 08:31:29'),
(2, 7, 13, 'Paper', 5, 'Abir', '5hhgro4 jocjwfw', 'afefvennows', '01711584950', 0.00, 3, 1, 7, 7, '2021-08-18 20:50:35', '2021-08-25 00:10:49'),
(3, 7, NULL, 'Clothings', 4, 'Mokbul Khan', 'Dhaka', 'Jashore', '01711584453', 0.00, -1, 1, 7, 7, '2021-08-26 16:26:46', '2021-08-26 16:34:46'),
(4, 7, 17, 'Gadget', 8, 'Habib', 'Dhaka', 'CTG', '01811345678', 0.00, 5, 1, 7, 7, '2021-08-26 16:33:53', '2021-08-26 16:44:04'),
(5, 7, 17, 'Paper', 2, 'Evan', 'mirpur', 'uttara', '01878123456', 0.00, 3, 1, 7, 7, '2021-08-26 16:49:31', '2021-08-28 06:22:00'),
(6, 7, 17, 'Gadget', 2, 'Rakib', 'Dhaka', 'ctg', '01744569996', 0.00, 3, 1, 7, 7, '2021-08-27 08:18:19', '2021-08-28 06:58:47'),
(7, 7, NULL, 'Paper', 2, 'Habib', 'jfoefqwf', 'fqweofqw', '01744569969', 0.00, -1, 1, 7, 7, '2021-08-27 08:18:57', '2021-08-27 08:19:12'),
(8, 7, 16, 'Clothings', 2, 'Mokbul Khan', 'fasfa', 'sfsaf', '01745789456', 0.00, 1, 1, 7, 7, '2021-08-27 08:50:59', '2021-08-27 08:58:18'),
(9, 7, NULL, 'Gadget', 3, 'eerefrfsdfdasf', 'fafafa', 'fewfwf', '01745789456', 0.00, -1, 1, 7, 7, '2021-08-27 08:51:24', '2021-08-27 12:44:36'),
(10, 7, 17, 'Clothings', 1, 'Rakib', 'wrew', 'wrwq', '01892123454', 0.00, 1, 1, 7, 7, '2021-08-27 12:45:16', '2021-08-27 12:46:36'),
(11, 7, 17, 'Gadget', 1, 'Rakib', 'fsafa', 'fasfa01988', '01911878787', 0.00, 5, 1, 7, 7, '2021-08-28 06:43:56', '2021-08-28 06:57:47'),
(12, 7, NULL, 'Clothings', 22, '22', '22', '222', '01977123456', 0.00, -1, 1, 7, 7, '2021-08-28 07:00:28', '2021-08-28 07:00:56');

-- --------------------------------------------------------

--
-- Table structure for table `order_action_statuses`
--

CREATE TABLE `order_action_statuses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `order_id` bigint(20) UNSIGNED DEFAULT NULL,
  `details` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `track_location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latitude` double(20,10) NOT NULL DEFAULT 0.0000000000,
  `longitude` double(20,10) NOT NULL DEFAULT 0.0000000000,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_action_statuses`
--

INSERT INTO `order_action_statuses` (`id`, `user_id`, `order_id`, `details`, `track_location`, `latitude`, `longitude`, `created_at`, `updated_at`) VALUES
(1, 16, 1, 'Product receive from customer.', 'Dhaka', 0.0000000000, 0.0000000000, '2021-08-25 00:16:05', '2021-08-25 00:16:05'),
(2, 16, 1, 'Product is on the way.', 'Dhaka', 0.0000000000, 0.0000000000, '2021-08-25 00:17:43', '2021-08-25 00:17:43'),
(3, 16, 1, 'Product is on the way.', 'Dhaka', 0.0000000000, 0.0000000000, '2021-08-25 00:17:47', '2021-08-25 00:17:47'),
(4, 17, 4, 'Product receive from customer.', 'Dhaka', 0.0000000000, 0.0000000000, '2021-08-26 16:41:54', '2021-08-26 16:41:54'),
(5, 17, 4, 'Product is on the way.', 'Dhaka', 0.0000000000, 0.0000000000, '2021-08-26 16:42:20', '2021-08-26 16:42:20'),
(6, 17, 4, 'Product is on the way.', 'Dhaka', 0.0000000000, 0.0000000000, '2021-08-26 16:43:54', '2021-08-26 16:43:54'),
(7, 17, 4, 'Product is on the way.', 'Dhaka', 0.0000000000, 0.0000000000, '2021-08-26 16:43:59', '2021-08-26 16:43:59'),
(8, 17, 4, 'Product is on the way.', 'Dhaka', 0.0000000000, 0.0000000000, '2021-08-26 16:44:02', '2021-08-26 16:44:02'),
(9, 17, 4, 'Product delivered.', 'Dhaka', 0.0000000000, 0.0000000000, '2021-08-26 16:44:04', '2021-08-26 16:44:04'),
(10, 17, 5, 'Product receive from customer.', 'Dhaka', 0.0000000000, 0.0000000000, '2021-08-26 16:51:02', '2021-08-26 16:51:02'),
(11, 17, 5, 'Product is on the way.', 'Dhaka', 0.0000000000, 0.0000000000, '2021-08-26 16:51:06', '2021-08-26 16:51:06'),
(12, 17, 5, 'Product delivered.', 'Dhaka', 0.0000000000, 0.0000000000, '2021-08-26 16:51:10', '2021-08-26 16:51:10'),
(14, 16, 1, 'Product is on the way.', 'Dhaka', 0.0000000000, 0.0000000000, '2021-08-27 08:30:43', '2021-08-27 08:30:43'),
(15, 16, 1, 'Product delivered.', 'Dhaka', 0.0000000000, 0.0000000000, '2021-08-27 08:31:29', '2021-08-27 08:31:29'),
(17, 17, 6, 'Product receive from customer.', 'Dhaka', 0.0000000000, 0.0000000000, '2021-08-27 09:09:22', '2021-08-27 09:09:22'),
(18, 17, 5, 'Product is on the way.', 'Dhaka', 0.0000000000, 0.0000000000, '2021-08-28 06:22:01', '2021-08-28 06:22:01'),
(19, 17, 11, 'Product receive from customer.', 'Dhaka', 0.0000000000, 0.0000000000, '2021-08-28 06:52:40', '2021-08-28 06:52:40'),
(20, 17, 11, 'Product is on the way.', 'Dhaka', 0.0000000000, 0.0000000000, '2021-08-28 06:57:42', '2021-08-28 06:57:42'),
(21, 17, 11, 'Product is on the way.', 'Dhaka', 0.0000000000, 0.0000000000, '2021-08-28 06:57:45', '2021-08-28 06:57:45'),
(22, 17, 11, 'Product delivered.', 'Dhaka', 0.0000000000, 0.0000000000, '2021-08-28 06:57:47', '2021-08-28 06:57:47'),
(23, 17, 6, 'Product is on the way.', 'Dhaka', 0.0000000000, 0.0000000000, '2021-08-28 06:58:47', '2021-08-28 06:58:47');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` int(11) NOT NULL COMMENT '1 => admin, 2 => deliveryMan, 3 => customer',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default.jpg',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `phone_no`, `address`, `photo`, `email_verified_at`, `password`, `remember_token`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Admin', 'admin@gmail.com', '01711678787', 'Dhaka', 'default.jpg', NULL, '$2y$10$Fflnolbi7if0HIoLppS/6eIdrM1FKRnd4hN9BJZjIGEIHO4LqUoga', NULL, 1, NULL, '2021-08-22 00:36:00'),
(2, 2, 'DeliveryMan', 'deliveryMan@gmail.coms', '01867789678', 'Dhaka', '1628926335.jpg', NULL, '$2y$10$3jLWgDh8Pjg7OL6huk7m8eK0Vwclr6Dd74rD1kTp4fe.kQO0.vQH6', NULL, 0, NULL, '2021-08-14 01:39:28'),
(3, 3, 'customer', 'customer@gmail.com', '01767789876', 'Chittagong', 'default.jpg', NULL, '$2y$10$3jLWgDh8Pjg7OL6huk7m8eK0Vwclr6Dd74rD1kTp4fe.kQO0.vQH6', NULL, 0, NULL, NULL),
(4, 3, 'cust 1', 'cust@gmail.com', '01711676543', 'Dhaka', 'default.jpg', NULL, '$2y$10$vH4ifGqmIvfRUpq1M9lXkefOh4v4ayDCpN0EL7XhT4/uqpnEQ6iem', NULL, 1, '2021-08-13 04:38:32', '2021-08-13 04:38:32'),
(5, 3, 'Customer 1', 'customer1@gmail.com', '01711123456', 'Dhaka', 'default.jpg', NULL, '$2y$10$vYNUykorKOBMBMHf1DZJ0.REidj8HCtSCPCktZDcPkdiKo36wdCf.', NULL, 1, '2021-08-13 04:40:18', '2021-08-13 04:40:18'),
(7, 3, 'customer100', 'customer100@gmail.com', '01678672333', 'Dhaka', 'default.jpg', NULL, '$2y$10$Kf.myjvBzO6yr9eCQFongOMWfqqmO0qlERdakuCO0GjPRsj/JJJH6', NULL, 1, '2021-08-13 04:45:28', '2021-08-22 00:37:31'),
(9, 3, 'chandu', 'abcde@gmail.com', '12345678900', 'Dhaka', 'default.jpg', NULL, '$2y$10$kRuisMKeI6QmGD724jJ4kOOCl7b9REC0P6kCzGCMQ1lVkYrOsYOoS', NULL, 1, '2021-08-13 13:08:05', '2021-08-13 13:08:05'),
(12, 3, 'customer Habib', 'abcd@gmail.com', '12345678900', 'Dhaka', 'default.jpg', NULL, '$2y$10$X5XyJuWeuw7KEiXCg/kZReEn3zP7qJ0WK4F60dR/HBdoNKJjdsXrm', NULL, 1, '2021-08-13 13:11:44', '2021-08-13 13:11:44'),
(13, 2, 'Akbar Khan', 'akiba@gmail.com', '01670461399', 'Dhaka CITY D', '1628926156.png', NULL, '$2y$10$T2SEYmrTnnTwXHef.eOg0uEEUfQXCAV4.LfF5gv2wpQreQqcui/Zq', NULL, 1, '2021-08-13 23:42:56', '2021-08-14 01:29:16'),
(16, 2, 'Motin', 'motin@gmail.com', '01745777777', 'Dhaka', '1628928916.jpg', NULL, '$2y$10$K7N33yQx8iTttmAmqppK6enntkg117LskgWzErJwYNKp4PAPeSdCK', NULL, 1, '2021-08-14 00:27:44', '2021-08-22 00:39:07'),
(17, 2, 'zakir', 'zk@gm.com', '01789129651', 'sfsf', '1628925816.jpg', NULL, '$2y$10$Ec0G2/b.zsldM4ccsS45suLJZgPBE3tr.6zWV0y5SsDLbJXnKvmfe', NULL, 1, '2021-08-14 01:05:58', '2021-08-14 04:58:30'),
(18, 1, 'New admin', 'newAdmin@gmail.com', '02789189951', 'Dhaka', 'default.jpg', NULL, '$2y$10$u3JZf97PFrDnS0YHi1DRe.uIOc8IkItdTztwC/1937rKxpte9JbCK', NULL, 1, '2021-08-14 04:19:32', '2021-08-14 04:23:04'),
(19, 1, 'hello Admin', 'taka@gmail.com', '01786678000', 'Address', 'default.jpg', NULL, '$2y$10$C0CvwDHkO/xqkLm7m9vhgeTQZu0PQJxa/izzCRDeihC2kPKIgh63u', NULL, 0, '2021-08-14 04:26:50', '2021-08-14 04:28:50'),
(20, 3, 'pkH1', 'pkH1@gmail.com', '12345678900', 'Dhaka', 'default.jpg', NULL, '$2y$10$6eGRCglrPyPUfaDSj/MHhe1dXZ.3oHvIafVg9QSX6JwWVzzUlM8.a', NULL, 1, '2021-08-20 12:40:11', '2021-08-20 12:40:11');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_customer_id_foreign` (`customer_id`),
  ADD KEY `orders_deliveryman_id_foreign` (`deliveryMan_id`);

--
-- Indexes for table `order_action_statuses`
--
ALTER TABLE `order_action_statuses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_action_statuses_user_id_foreign` (`user_id`),
  ADD KEY `order_action_statuses_order_id_foreign` (`order_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `order_action_statuses`
--
ALTER TABLE `order_action_statuses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `orders_deliveryman_id_foreign` FOREIGN KEY (`deliveryMan_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `order_action_statuses`
--
ALTER TABLE `order_action_statuses`
  ADD CONSTRAINT `order_action_statuses_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `order_action_statuses_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
